/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author STDG_077
 */
public class Lab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();

            if (isWin()) {
                printTable();
                if (isTie()) {
                    System.out.println("It's a DRAW!");
                } else {
                    printWin();
                }
                break;
            }
            switchPlayer();
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }

    }

    private static void printTurn() {
        System.out.println("Player " + currentPlayer + " turn");
    }

    private static void inputRowCol() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Please input Number: ");
            row = scanner.nextInt();
            col = scanner.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;

                return;
            }
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        } else if (isTie()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " WIN!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int j = 0; j < 3; j++) {
            if (table[col - 1][j] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean isTie() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
